package com.example.lioxo.bulbi.adapters;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.lioxo.bulbi.R;

import java.util.HashMap;

public class BulbScanAdapter extends ArrayAdapter<BluetoothDevice>{
    private HashMap<String, BluetoothDevice> bulbList = new HashMap<>();

    public BulbScanAdapter(@NonNull Context context, int resource) {
        super(context, resource);
    }

    public void addBulb(BluetoothDevice bulb){
        this.clear();
        String key = bulb.getAddress();
        this.bulbList.put(key, bulb);
        this.addAll(this.bulbList.values());
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        BluetoothDevice device = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.bulb_scan_item, parent, false);
        }
        TextView bulbName = convertView.findViewById(R.id.bulb_name);
        if(device != null){
            bulbName.setText(device.getName());
        }
        return convertView;
    }
}
