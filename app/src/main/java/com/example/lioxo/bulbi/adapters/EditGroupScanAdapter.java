package com.example.lioxo.bulbi.adapters;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.lioxo.bulbi.business.EditGroupListItem;
import com.example.lioxo.bulbi.R;
import com.example.lioxo.bulbi.entities.Bulb;

import java.util.ArrayList;
import java.util.HashMap;


public class EditGroupScanAdapter extends ArrayAdapter<EditGroupListItem> {
    private HashMap<String, EditGroupListItem> editGroupListItems = new HashMap<>();
    private EditGroupScanAdapter adapter = this;

    public EditGroupScanAdapter(@NonNull Context context, int resource) {
        super(context, resource);
    }

    public void addBulb(BluetoothDevice bulb, boolean isSelected) {
        EditGroupListItem editGroupListItem = new EditGroupListItem(bulb, isSelected);
        editGroupListItems.put(editGroupListItem.getBluetoothDevice().getAddress(), editGroupListItem);
        this.clear();
        this.addAll(editGroupListItems.values());
        this.notifyDataSetChanged();
    }

    public boolean isBulbAlreadyListed(String address){
        return editGroupListItems.keySet().contains(address);
    }

    public ArrayList<Bulb> getSelectedBulbs(){
        ArrayList<Bulb> bulbs = new ArrayList<>();
        for(EditGroupListItem editGroupListItem : editGroupListItems.values()){
            if(editGroupListItem.isSelected()){
                Bulb bulb = new Bulb();
                bulb.address = editGroupListItem.getBluetoothDevice().getAddress();
                bulbs.add(bulb);
            }
        }
        return bulbs;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.edit_group_scan_item, parent, false);
        }
        final EditGroupListItem editGroupListItem = this.getItem(position);
        if (editGroupListItem != null) {
            final CheckBox checkBox = convertView.findViewById(R.id.edit_group_listitem_checkbox);
            checkBox.setTag(editGroupListItem.getBluetoothDevice().getAddress());
            checkBox.setChecked(editGroupListItem.isSelected());
            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    editGroupListItem.setSelected(!editGroupListItem.isSelected());
                    editGroupListItems.put(editGroupListItem.getBluetoothDevice().getAddress(), editGroupListItem);
                    adapter.clear();
                    adapter.addAll(editGroupListItems.values());
                    adapter.notifyDataSetChanged();
                }
            });
            TextView bulbName = convertView.findViewById(R.id.bulb_name);
            bulbName.setText(editGroupListItem.getBluetoothDevice().getName());
        }

        return convertView;
    }
}
