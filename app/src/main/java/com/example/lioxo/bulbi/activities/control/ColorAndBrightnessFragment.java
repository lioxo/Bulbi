package com.example.lioxo.bulbi.activities.control;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.lioxo.bulbi.R;
import com.example.lioxo.bulbi.business.ColorConverter;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorChangedListener;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.slider.LightnessSlider;


public class ColorAndBrightnessFragment extends Fragment {
    ColorPickerView colorPicker;
    LightnessSlider lightnessSlider;
    ControllerInterface activity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (ControllerInterface) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_color_and_brightness, container, false);
        colorPicker = view.findViewById(R.id.color_picker);
//        colorPicker.setColor(ColorConverter.convertFromByteArrayToInteger(activity.getController().getColor()), true);
        colorPicker.addOnColorChangedListener(new OnColorChangedListener() {
            @Override
            public void onColorChanged(int color) {
                activity.getController().setColor(color);
            }
        });
        colorPicker.addOnColorSelectedListener(new OnColorSelectedListener() {
            @Override
            public void onColorSelected(int color) {
                activity.getController().setColor(color);
            }
        });
        lightnessSlider = view.findViewById(R.id.lightness_slider);
        return view;
    }
}
