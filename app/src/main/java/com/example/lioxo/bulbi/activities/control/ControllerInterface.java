package com.example.lioxo.bulbi.activities.control;

import com.example.lioxo.bulbi.business.Controller;

public interface ControllerInterface {
    public Controller getController();
}
