package com.example.lioxo.bulbi.business;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.ArrayList;

public class Controller {
    public final static String BROADCAST_READY_FOR_CONTROL = Controller.class.getCanonicalName().concat(".BROADCAST_READY_FOR_CONTROL");

    private ArrayList<BluetoothGatt> gatts = new ArrayList<>();
    private byte[] color;
    private byte[] effect;
    private Context applicationContext;

    public Controller(ArrayList<BluetoothDevice> devices, Context context){
        applicationContext = context;
        for(BluetoothDevice device : devices){
            device.connectGatt(context, false, new MyBluetoothCallback());
        }
    }

    private class MyBluetoothCallback extends BluetoothGattCallback{
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);
            if(newState == BluetoothGatt.STATE_CONNECTED){
                gatts.add(gatt);
                gatt.discoverServices();
            }
            if(newState == BluetoothGatt.STATE_DISCONNECTED){
                gatts.remove(gatt);
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);
            gatt.readCharacteristic(gatt.getService(UUIDContainer.serviceUUID).getCharacteristic(UUIDContainer.characteristicColorAndBrightness));
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicRead(gatt, characteristic, status);
            if(characteristic.getUuid().equals(UUIDContainer.characteristicColorAndBrightness)){
                if(color == null){
                    color = characteristic.getValue();
                    gatt.readCharacteristic(gatt.getService(UUIDContainer.serviceUUID).getCharacteristic(UUIDContainer.characteristicEffects));
                } else {
                    characteristic.setValue(color);
                    gatt.writeCharacteristic(characteristic);
                }
            } else if (characteristic.getUuid().equals(UUIDContainer.characteristicEffects)){
                if(effect == null){
                    effect = characteristic.getValue();
                } else {
                    characteristic.setValue(effect);
                    gatt.writeCharacteristic(characteristic);
                }
            }
            if(color != null && effect != null){
                Intent intent = new Intent();
                intent.setAction(BROADCAST_READY_FOR_CONTROL);
                applicationContext.sendBroadcast(intent);
            }
        }
    }

    public void setColor(int colorAsInt){
        byte[] colorAsByteArray = ColorConverter.convertFromIntegerToByteArray(colorAsInt);
        for(BluetoothGatt gatt : gatts){
            BluetoothGattCharacteristic characteristic = gatt.getService(UUIDContainer.serviceUUID).getCharacteristic(UUIDContainer.characteristicColorAndBrightness);
            characteristic.setValue(colorAsByteArray);
            gatt.writeCharacteristic(characteristic);
        }
    }

    public byte[] getColor(){
        return color;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        applicationContext = null;
        for (BluetoothGatt gatt : gatts){
            gatt.close();
        }
    }
}
