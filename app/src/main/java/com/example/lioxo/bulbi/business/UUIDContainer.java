package com.example.lioxo.bulbi.business;

import java.util.UUID;

public class UUIDContainer {
    public final static UUID serviceUUID = UUID.fromString("0000ff01-0000-1000-8000-00805f9b34fb");
    public final static UUID characteristicEffects= UUID.fromString("0000fffb-0000-1000-8000-00805f9b34fb");
    public final static UUID characteristicColorAndBrightness = UUID.fromString("0000fffc-0000-1000-8000-00805f9b34fb");
}
