package com.example.lioxo.bulbi.business;

import java.math.BigInteger;
import java.nio.ByteBuffer;

public class ColorConverter {
    public static int convertFromByteArrayToInteger(byte[] bulbColor) {
        int white = bulbColor[0] & 0xff;
        int red = bulbColor[1] & 0xff;
        int green = bulbColor[2] & 0xff;
        int blue = bulbColor[3] & 0xff;
        int color;
        if (white > 0) {
            color = new BigInteger("ffffffff", 16).intValue();
        } else {
            String redHex = Integer.toHexString(red);
            String greenHex = Integer.toHexString(green);
            String blueHex = Integer.toHexString(blue);
            String colorHex = "ff" + redHex + greenHex + blueHex;
            color = new BigInteger(colorHex, 16).intValue();
        }
        return color;
    }

    public static byte[] convertFromIntegerToByteArray(int intColor) {
        String colorHex = Integer.toHexString(intColor);
        String redHex = colorHex.substring(2, 4);
        String greenHex = colorHex.substring(4, 6);
        String blueHex = colorHex.substring(6, 8);
        int colorToBeSet;
        if (!(redHex.equals(greenHex) && greenHex.equals(blueHex))) {
            String hexString = "00".concat(redHex).concat(greenHex).concat(blueHex);
            colorToBeSet = new BigInteger(hexString, 16).intValue();
        } else {
            String hexString = redHex.concat("000000");
            colorToBeSet = new BigInteger(hexString, 16).intValue();
        }
        return ByteBuffer.allocate(4).putInt(colorToBeSet).array();
    }
}
