package com.example.lioxo.bulbi.business;

import android.bluetooth.BluetoothDevice;

public class EditGroupListItem {
    private BluetoothDevice mBluetoothDevice;
    private boolean isSelected = false;

    public EditGroupListItem(BluetoothDevice bluetoothDevice, boolean isSelected) {
        this.mBluetoothDevice = bluetoothDevice;
        this.isSelected = isSelected;
    }

    public BluetoothDevice getBluetoothDevice() {
        return mBluetoothDevice;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
