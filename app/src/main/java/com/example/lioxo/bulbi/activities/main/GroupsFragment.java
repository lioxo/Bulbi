package com.example.lioxo.bulbi.activities.main;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.lioxo.bulbi.R;
import com.example.lioxo.bulbi.activities.control.ControlActivity;
import com.example.lioxo.bulbi.activities.edit_group.EditGroupActivity;
import com.example.lioxo.bulbi.business.BluetoothAdapterFactory;
import com.example.lioxo.bulbi.entities.Bulb;
import com.example.lioxo.bulbi.entities.Group;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;


public class GroupsFragment extends Fragment {
    private ArrayAdapter<Group> groupListAdapter;
    private Realm realm;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Realm.init(getActivity());
        realm = Realm.getDefaultInstance();
        groupListAdapter = new GroupListAdapter(getContext(), R.layout.group_list_item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_groups, container, false);
        initializeFabButton(view);
        initializeListview(view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        queryForGroups();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    private void initializeFabButton(View view) {
        FloatingActionButton fabButton = view.findViewById(R.id.add_group_button);
        fabButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), EditGroupActivity.class);
                startActivity(intent);
            }
        });
    }

    private void initializeListview(View view) {
        ListView listView = view.findViewById(R.id.list_groups);
        listView.setAdapter(groupListAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Group group = (Group) adapterView.getItemAtPosition(i);
                if (group != null) {
                    BluetoothAdapter bluetoothAdapter = BluetoothAdapterFactory.getBluetoothAdapter(getActivity());
                    ArrayList<BluetoothDevice> devices = new ArrayList<>();
                    for (Bulb bulb : group.bulbs) {
                        BluetoothDevice device = bluetoothAdapter.getRemoteDevice(bulb.address);
                        devices.add(device);
                    }
                    Intent intent = new Intent(getActivity(), ControlActivity.class);
                    intent.putParcelableArrayListExtra(ControlActivity.EXTRA_ARRAYLIST_BLUETOOTHDEVICES, devices);
                    startActivity(intent);
                }
            }
        });
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                Group group = (Group) adapterView.getItemAtPosition(i);
                Intent intent = new Intent(getActivity(), EditGroupActivity.class);
                intent.putExtra(EditGroupActivity.EXTRA_GROUP, group.id);
                startActivity(intent);
                return true;
            }
        });
    }

    private void queryForGroups() {
        RealmResults<Group> groups = realm.where(Group.class).findAll();
        groupListAdapter.clear();
        groupListAdapter.addAll(groups);
        groupListAdapter.notifyDataSetChanged();
    }

    private class GroupListAdapter extends ArrayAdapter<Group> {
        GroupListAdapter(@NonNull Context context, int resource) {
            super(context, resource);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(this.getContext()).inflate(R.layout.group_list_item, parent, false);
            }
            Group group = this.getItem(position);
            if (group != null) {
                TextView groupName = convertView.findViewById(R.id.group_name);
                groupName.setText(group.name);
            }
            return convertView;
        }
    }
}
