package com.example.lioxo.bulbi.activities.main;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.ParcelUuid;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.lioxo.bulbi.R;
import com.example.lioxo.bulbi.activities.control.ControlActivity;
import com.example.lioxo.bulbi.adapters.BulbScanAdapter;
import com.example.lioxo.bulbi.business.BluetoothAdapterFactory;
import com.example.lioxo.bulbi.business.UUIDContainer;

import java.util.ArrayList;
import java.util.UUID;

public class BulbsFragment extends Fragment {
    BulbScanAdapter bulblistAdapter;

    private boolean isScanning = false;

    @Override
    public void onStart() {
        super.onStart();
        startScan();
    }

    @Override
    public void onStop() {
        super.onStop();
        bulblistAdapter.clear();
        stopScan();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bulbs, container, false);
        ListView bulbList = view.findViewById(R.id.bulb_list);
        bulblistAdapter = new BulbScanAdapter(getContext(), R.layout.bulb_scan_item);
        bulbList.setAdapter(bulblistAdapter);
        bulbList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                BulbScanAdapter bulbScanAdapter = (BulbScanAdapter) adapterView.getAdapter();
                BluetoothDevice device = bulbScanAdapter.getItem(i);
                ArrayList<BluetoothDevice> devices = new ArrayList<>();
                devices.add(device);
                Intent intent = new Intent(getContext(), ControlActivity.class);
                intent.putParcelableArrayListExtra(ControlActivity.EXTRA_ARRAYLIST_BLUETOOTHDEVICES, devices);
                startActivity(intent);
            }
        });
        return view;
    }

    private void startScan() {
        if (isScanning) {
            return;
        }
        BluetoothAdapter bluetoothAdapter = BluetoothAdapterFactory.getBluetoothAdapter(getActivity());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            BluetoothLeScanner bluetoothLeScanner = bluetoothAdapter.getBluetoothLeScanner();
            ArrayList<ScanFilter> scanFilters = new ArrayList<>();
            scanFilters.add(new ScanFilter.Builder().setServiceUuid(new ParcelUuid(UUIDContainer.serviceUUID)).build());
            ScanSettings scanSettings = new ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_LOW_POWER).build();
            bluetoothLeScanner.startScan(scanFilters, scanSettings, scanCallback);
            isScanning = true;
        } else {
            UUID[] uuids = new UUID[]{UUIDContainer.serviceUUID};
            bluetoothAdapter.startLeScan(uuids, leScanCallback);
            isScanning = true;
        }
    }

    private void stopScan() {
        if (isScanning) {
            BluetoothAdapter bluetoothAdapter = BluetoothAdapterFactory.getBluetoothAdapter(getActivity());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                BluetoothLeScanner bluetoothLeScanner = bluetoothAdapter.getBluetoothLeScanner();
                bluetoothLeScanner.stopScan(scanCallback);
            } else {
                bluetoothAdapter.stopLeScan(leScanCallback);
            }
            isScanning = false;
        }
    }

    private ScanCallback scanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                bulblistAdapter.addBulb(result.getDevice());
            }
        }
    };

    private BluetoothAdapter.LeScanCallback leScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(BluetoothDevice bluetoothDevice, int i, byte[] bytes) {
            bulblistAdapter.addBulb(bluetoothDevice);
        }
    };
}
