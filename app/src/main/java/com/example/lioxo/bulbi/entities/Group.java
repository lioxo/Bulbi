package com.example.lioxo.bulbi.entities;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class Group extends RealmObject{
    @PrimaryKey
    public long id;
    @Required
    public String name;
    public RealmList<Bulb> bulbs;
}
