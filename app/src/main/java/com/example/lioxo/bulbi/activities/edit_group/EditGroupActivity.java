package com.example.lioxo.bulbi.activities.edit_group;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.ParcelUuid;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lioxo.bulbi.R;
import com.example.lioxo.bulbi.adapters.EditGroupScanAdapter;
import com.example.lioxo.bulbi.business.BluetoothAdapterFactory;
import com.example.lioxo.bulbi.business.UUIDContainer;
import com.example.lioxo.bulbi.entities.Bulb;
import com.example.lioxo.bulbi.entities.Group;

import java.util.ArrayList;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmList;

public class EditGroupActivity extends AppCompatActivity {
    public final static String EXTRA_GROUP = EditGroupActivity.class + ".EXTRA_GROUP";

    private Group group;
    private boolean isScanning = false;
    private EditGroupScanAdapter bulbListAdapter;
    private TextView textViewGroupName;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_group);
        Realm.init(this);
        realm = Realm.getDefaultInstance();
        bulbListAdapter = new EditGroupScanAdapter(this, R.layout.edit_group_scan_item);
        textViewGroupName = findViewById(R.id.group_name);
        ListView listView = findViewById(R.id.bulb_list);
        listView.setAdapter(bulbListAdapter);
        initializeSaveButton();
    }

    private void prePopulateSelectedBulbsOfGroup(){
        long groupId = getIntent().getLongExtra(EXTRA_GROUP, -1);
        Group group = realm.where(Group.class).equalTo("id", groupId).findFirst();
        this.group = group;
        if(group != null){
            textViewGroupName.setText(group.name);
            BluetoothAdapter bluetoothAdapter = BluetoothAdapterFactory.getBluetoothAdapter(this);
            bulbListAdapter.clear();
            for(Bulb bulb : group.bulbs){
                bulbListAdapter.addBulb(bluetoothAdapter.getRemoteDevice(bulb.address), true);
            }
            bulbListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        prePopulateSelectedBulbsOfGroup();
        startScan();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopScan();
    }

    private void startScan() {
        if(isScanning){
            return;
        }
        BluetoothAdapter bluetoothAdapter = BluetoothAdapterFactory.getBluetoothAdapter(this);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            BluetoothLeScanner bluetoothLeScanner = bluetoothAdapter.getBluetoothLeScanner();
            ScanFilter scanFilter = new ScanFilter.Builder().setServiceUuid(new ParcelUuid(UUIDContainer.serviceUUID)).build();
            ArrayList<ScanFilter> scanFilters = new ArrayList<>();
            scanFilters.add(scanFilter);
            ScanSettings scanSettings = new ScanSettings.Builder().build();
            bluetoothLeScanner.startScan(scanFilters, scanSettings, scanCallback);
        } else {
            UUID[] serviceUUIDs = new UUID[]{UUIDContainer.serviceUUID};
            bluetoothAdapter.startLeScan(serviceUUIDs, leScanCallback);
        }
    }

    private void stopScan() {
        if(!isScanning){
            return;
        }
        BluetoothAdapter bluetoothAdapter = BluetoothAdapterFactory.getBluetoothAdapter(this);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            BluetoothLeScanner bluetoothLeScanner = bluetoothAdapter.getBluetoothLeScanner();
            bluetoothLeScanner.stopScan(scanCallback);
        } else {
            bluetoothAdapter.stopLeScan(leScanCallback);
        }
    }

    private BluetoothAdapter.LeScanCallback leScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(BluetoothDevice bluetoothDevice, int i, byte[] bytes) {
            addDeviceToList(bluetoothDevice);
        }
    };

    private ScanCallback scanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
                addDeviceToList(result.getDevice());
            }
        }
    };

    private void initializeSaveButton() {
        FloatingActionButton saveButton = findViewById(R.id.edit_group_done_button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(textViewGroupName.getText().toString().trim().length() == 0){
                    Toast.makeText(EditGroupActivity.this, getString(R.string.group_name_cannot_be_empty), Toast.LENGTH_SHORT).show();
                    return;
                }
                if(bulbListAdapter.getSelectedBulbs().size() == 0){
                    Toast.makeText(EditGroupActivity.this, R.string.must_at_least_select_one_bulb, Toast.LENGTH_SHORT).show();
                    return;
                }

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(@NonNull Realm realm) {
                        if(group == null){
                            Number maxId = realm.where(Group.class).max("id");
                            long nextId = (maxId == null) ? 0 : maxId.intValue() + 1;
                            group = realm.createObject(Group.class, nextId);
                        }
                        group.name = textViewGroupName.getText().toString();
                        group.bulbs = new RealmList<>();
                        group.bulbs.addAll(bulbListAdapter.getSelectedBulbs());
                        realm.insertOrUpdate(group);
                    }
                });
                Intent intent = new Intent(EditGroupActivity.this, com.example.lioxo.bulbi.activities.main.MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }

    private void addDeviceToList(BluetoothDevice device) {
        if(!bulbListAdapter.isBulbAlreadyListed(device.getAddress())){
            bulbListAdapter.addBulb(device, false);
        }
    }
}
