package com.example.lioxo.bulbi.activities.control;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.example.lioxo.bulbi.R;
import com.example.lioxo.bulbi.business.Controller;

import java.util.ArrayList;

public class ControlActivity extends AppCompatActivity implements ControllerInterface {
    public final static String EXTRA_ARRAYLIST_BLUETOOTHDEVICES = ControlActivity.class.getCanonicalName().concat("_EXTRA_ARRAYLIST_BLUETOOTHDEVICES");

    private ArrayList<BluetoothDevice> devices = new ArrayList<>();
    private Controller controller;
    private BroadcastReceiver broadcastReceiver;
    private BottomNavigationView navigation;


    private BottomNavigationView.OnNavigationItemSelectedListener navigationItemListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            Fragment previousFragment = fragmentManager.findFragmentById(R.id.content_layout_color_and_effects);
            if (previousFragment != null) fragmentTransaction.remove(previousFragment);
            switch (item.getItemId()) {
                case R.id.navigation_color_and_brightness:
                    fragmentTransaction.add(R.id.content_layout_color_and_effects, new ColorAndBrightnessFragment());
                    fragmentTransaction.commit();
                    return true;
                case R.id.navigation_effecs:
                    fragmentTransaction.add(R.id.content_layout_color_and_effects, new EffectsFragment());
                    fragmentTransaction.commit();
                    return true;
            }
            fragmentTransaction.commit();
            return false;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control);

        Intent callingIntent = getIntent();
        devices = callingIntent.getParcelableArrayListExtra(EXTRA_ARRAYLIST_BLUETOOTHDEVICES);

        broadcastReceiver = new ControlBroadcastReceiver();

        navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(navigationItemListener);
    }

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter intentFilter = new IntentFilter(Controller.BROADCAST_READY_FOR_CONTROL);
        this.registerReceiver(broadcastReceiver, intentFilter);
        controller = new Controller(devices, this.getApplicationContext());
    }

    @Override
    protected void onStop() {
        super.onStop();
        controller = null;
    }

    @Override
    public Controller getController() {
        return controller;
    }

    private class ControlBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            findViewById(R.id.controller_loading).setVisibility(View.GONE);
            navigation.setSelectedItemId(R.id.navigation_color_and_brightness);
            unregisterReceiver(broadcastReceiver);
        }
    }
}
