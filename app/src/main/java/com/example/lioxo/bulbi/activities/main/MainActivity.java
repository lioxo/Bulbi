package com.example.lioxo.bulbi.activities.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.example.lioxo.bulbi.R;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(new NavigationItemSelectedListener());
        navigation.setSelectedItemId(R.id.navigation_groups);
    }

    private class NavigationItemSelectedListener implements BottomNavigationView.OnNavigationItemSelectedListener{
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            Fragment previousFragment = fragmentManager.findFragmentById(R.id.content_layout_groups_and_bulbs);
            if (previousFragment != null) fragmentTransaction.remove(previousFragment);
            switch (item.getItemId()) {
                case R.id.navigation_groups:
                    Fragment groupsFragment = new GroupsFragment();
                    fragmentTransaction.add(R.id.content_layout_groups_and_bulbs, groupsFragment);
                    fragmentTransaction.commit();
                    return true;
                case R.id.navigation_bulbs:
                    Fragment bulbsFragment = new BulbsFragment();
                    fragmentTransaction.add(R.id.content_layout_groups_and_bulbs, bulbsFragment);
                    fragmentTransaction.commit();
                    return true;
                case R.id.navigation_settings:
                    Fragment settingsFragment = new SettingsFragment();
                    fragmentTransaction.add(R.id.content_layout_groups_and_bulbs, settingsFragment);
                    fragmentTransaction.commit();
                    return true;
            }
            return false;
        }
    }
}
