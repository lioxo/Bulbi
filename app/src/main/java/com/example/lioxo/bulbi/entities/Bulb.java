package com.example.lioxo.bulbi.entities;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Bulb extends RealmObject{
    @PrimaryKey
    public String address;
}
